import cv2
from base64 import b64encode
from PIL import Image
from PIL import ImageFilter
import googleapiclient.discovery
from oauth2client.client import GoogleCredentials
import time
import re
from imutils.video import VideoStream
import argparse
import datetime
from picamera import PiCamera
from picamera.array import PiRGBArray

from google.cloud import vision
from google.cloud.vision import types

from database import *
import os
import autofocus

CREDENTIALS_FILE = "/home/pi/Documents/Capstone-Group6/credentials.json"
DEBUG = True
# def capture_image():
# 	camera.rotation = 270
# 	camera.capture('/home/pi/Documents/Test_Bar/temp_image.jpg')

# def detect_expiration_date_deprecated(frame):
	
# 	cv2.imwrite('/home/pi/Documents/Test_Bar/temp_image.jpg',frame)
# 	#-------------------------------------- EXPIRATION DATE-----------------------------
	
# 	start = time.time()
# 	FILE = "/home/pi/Documents/Test_Bar/temp_image" # don't use cropped image
# 	IMAGE_FILE = FILE+".jpg"
# 	IMAGE_FILE_SMOOTH = FILE+"_smooth.jpg"
# 	IMAGE_FILE_SCALED = FILE+"_scaled.jpg"
# 	# CREDENTIALS_FILE = "credentials.json"

# 	#Image for processing
# 	IMAGE = IMAGE_FILE_SCALED

# 	#downsize jpeg image
# 	foo = Image.open(IMAGE_FILE)
# 	foo = foo.filter(ImageFilter.SMOOTH)
# 	# foo = foo.rotate(270,expand=True)
# 	# foo.save(IMAGE_FILE_SCALED,optimize=True,quality=85)
# 	foo.save(IMAGE_FILE_SCALED,optimize=True,quality=50)
# 	# foo.save(IMAGE_FILE_SMOOTH,optimize=True)
	
# 	vision_credentials = GoogleCredentials.from_stream(CREDENTIALS_FILE)
# 	service = googleapiclient.discovery.build('vision', 'v1', credentials=vision_credentials)
	
# 	# Setup Firebase App
# 	db = initializeDB()
	
# 	with open(IMAGE, "rb") as f:
# 		image_data = f.read()
# 		encoded_image_data = b64encode(image_data).decode('UTF-8')

# 	#create request object for google vision API
# 	batch_request = [{

# 		'image': {
# 		    'content': encoded_image_data
# 		},
# 		'features': [
# 		    {
# 		        'type': 'TEXT_DETECTION'
# 		    }
# 		]
# 	}]
# 	request = service.images().annotate(body={'requests' : batch_request})
# 	#Send request to Google
# 	response = request.execute()
	
# 	#check for errors
	
# 	if 'error' in response:
# 		raise RuntimeError(response['error'])

# 	if 'textAnnotations' in response['responses'][0]:
# 		extracted_texts = response['responses'][0]['textAnnotations']

# 		#----- TEST CASES ------#

# 		# EXP 2016 MAR 13
# 		# EXP 13 MAR 2016
# 		# EXP 16MA13   (YY/MM/DD)
# 		# EXP 17 AL 30 (/YY/MM/DD)
# 		# EXP /01/12/2019 (/MM/DD/YYYY)
# 		# EXP /13/01/2019 (/DD/MM/YYYY)
# 		# EXP /2019/12/10 (/YYYY/MM/DD)
# 		# EXP 03 JAN 2010

# 		#------------------------#
# 		#----- NOT SUPPORTED FORMATS----#
# 		# Dec 15 2019

# 		print(extracted_texts[0]['description'])
# 		exp_date = re.search("(\d{2}|20\d{2})\s?[a-zA-Z]{2,3}\s?\d{2,4}|\W\d{2,4}\W\d{2}\W\d{2,4}", extracted_texts[0]['description'])							

# 		if exp_date:
# 			print("Expiry Date:")
# 			print(exp_date.group(0))
		
# 		updateDB(db, exp_date.group(0))
# 		end = time.time()
# 		time_elapsed =  end - start
# 		print("Total Time Elapsed: ", time_elapsed)
# 	#returns the name of the product. Will return
# 	else:
# 		print('no text detected')

# Initializes vision client
# Returns client instance	
def initializeVisionClient():
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"]=CREDENTIALS_FILE
	return vision.ImageAnnotatorClient()
	
# Sends request to Vision client for text extraction from image
# Returns all text found in image
def detect_text(path, client):
	#start = time.time()
	with open(path, 'rb') as img:
		content = img.read()
	if DEBUG:
		print("[INFO] Sending Frame to Google for Processing..")
	image = types.Image(content=content)
	response = client.text_detection(image=image)
	texts = response.text_annotations
	string = ''

	for text in texts:
		string+=' '+text.description
	#end = time.time()
	#print('Single Google Request: ', end - start)
	return string
	
#----- TEST CASES ------#

	# EXP 2016 MAR 13
	# EXP 13 MAR 2016
	# EXP 16MA13   (YY/MM/DD)
	# EXP 17 AL 30 (/YY/MM/DD)
	# EXP /01/12/2019 (/MM/DD/YYYY)
	# EXP /13/01/2019 (/DD/MM/YYYY)
	# EXP /2019/12/10 (/YYYY/MM/DD)
	# EXP 03 JAN 2010

	#------------------------#
	#----- NOT SUPPORTED FORMATS----#
	# Dec 15 2019

# Parse all text detected by vision for the expiry date
# Returns the expiry date if found, otherwise returns nothing

def parse_text(text):
	if DEBUG:
		print("[INFO] Parsing text for expiry date..")
	exp_date = re.search("(\d{2}|20\d{2})\s?[a-zA-Z]{2,4}\s?\d{2,4}|\W\d{2,4}\W\d{2}\W\d{2,4}", text)							
	message = ''

	if exp_date:
		message = exp_date.group(0)
	else:
		#raise ValueError("No expiry date found")
		message = "ERROR"
	
	return message
	
# Main 
#if __name__ == '__main__':
#	camera = PiCamera()
#	client = initializeVisionClient()
#Infinite Loop: Press 1 to start process
	# initialize the video stream and allow the camera sensor to warm up
	#print("[INFO] starting video stream...")
	#camera = PiCamera()
	#camera.resolution = (640, 480)
	#camera.framerate = 32
	#rawCapture = PiRGBArray(camera, size=(640, 480))	# vs = VideoStream(src=0).start()
	
	#time.sleep(2.0)
	## loop over the frames from the video stream
	#for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
		#image = frame.array
 
		##frame = vs.read()
		#cv2.imshow("Expiration Scanner", image)
		#key = cv2.waitKey(1) & 0xFF
		#if key == ord("1"):
			#cv2.imwrite('/home/pi/Documents/Test_Bar/temp_image.jpg',image)
			#detect_expiration_date(image)
		#rawCapture.truncate(0)
	#autofocus.autofocus_camera(camera)
	#camera.close()
	#vs = VideoStream(usePiCamera = True).start()
	#time.sleep(1)
	
#	while True:
#		frame = vs.read()
#		file = '/home/pi/Documents/Test_Bar/temp_image.jpg'
#		cv2.imwrite(file,frame)
#		cv2.imshow("Expiration Scanner", frame)
#		print(detect_text(file,client))
		
	
