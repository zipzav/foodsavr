
/*Setting Up Google's Firebase-----------------------------------------*/
var user_key = "DwPmSihSEJiwpZDycUnu";
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAnF0YOx-ZMNZzzssC36CN5lGEPGL-OWGk",
    authDomain: "foodsavr-9ac98.firebaseapp.com",
    databaseURL: "https://foodsavr-9ac98.firebaseio.com",
    projectId: "foodsavr-9ac98",
    storageBucket: "foodsavr-9ac98.appspot.com",
    messagingSenderId: "492117207441",
    appId: "1:492117207441:web:14ea9e129ee934d3"
  };
 
// Initializing Firebase
firebase.initializeApp(firebaseConfig);
//Initializing Firestore (The Actual Database)
var db = firebase.firestore();
// Listens for any changes to database and appends new row for each item
let query = db.collection("users").doc("DwPmSihSEJiwpZDycUnu").collection("scanned_items")
	let observer = query.onSnapshot(querySnapshot => {
		console.log(`Received query snapshot of size ${querySnapshot.size}`);
		console.log("Successfully listened for item")
		querySnapshot.docChanges().forEach(change => {
			if (change.type == 'added') {
				console.log('New item: ', change.doc.data())
				append_row(change.doc.data(), change.doc.id)
			}
		});
		//retrieve_scanned_items()
	  }, err => {
		console.log(`Encountered error: ${err}`);
	  });
/*---------------------------------------------------------------------*/

/* Query the databse for scanned item list, then generate the table*/
//TO-DO: ERROR CHECKING IF DATA GET FAILED
function retrieve_scanned_items() {
	this.scanned_items = []
	var scanned_items_ref = db.collection("users").doc("DwPmSihSEJiwpZDycUnu").collection("scanned_items");
	var scanned_items = scanned_items_ref.get()
		.then( (snapshot) => {
			snapshot.forEach( (doc) => {			
				//this.scanned_items.push(doc.data());
				var item = doc.data();
				append_row(item, doc.id);
				//this.food_item_table.push(new food_item(item.amount, item.expiration_date, item.name, item.scanned_date, item.upc, doc.id));
			});
		})
		//this happens after collecting all item data
		.then(()  => {

		});	
}

/* Check for updates to database and refresh the page*/

function new_item_listener2(){
	let query = db.collection("users").doc("DwPmSihSEJiwpZDycUnu")
	let observer = query.onSnapshot(querySnapshot => {
		console.log(`Received query snapshot of size ${querySnapshot.size}`);
		console.log("Successfully listened for item")
	  }, err => {
		console.log(`Encountered error: ${err}`);
	  });
}
  


// Listen for any change on document `marie` in collection `users`
function new_item_listener(){ 
	firebase.firestore().document("users/DwPmSihSEJiwpZDycUnu").onWrite((change, context) => {
	  // ... Your code here
	  console.log("Successfully listened for item");
	});
}

/*Generate the table from database
inputs: item - document.data of item
		key - document.id 
*/
function append_row(item, key) {
  // get the reference for the body
  var main_table = document.getElementById('main_table').getElementsByTagName('tbody')[0];

  // creating all cells
    // creates a table row
    var row = document.createElement("tr");
	
	//<th scope="col">Item</th>
	//var cell = row.insertCell();
	//cell.innerHTML = main_table.rows.length + 1;
	
	//<th scope="col">Name</th>
  	var cell = row.insertCell();
	cell.innerHTML = item.name;
	
	//<th scope="col">Amount</th>
	var cell = row.insertCell();
	cell.innerHTML = item.amount;

	
	//<th scope="col">Scanned Date</th>
	var cell = row.insertCell();
	cell.innerHTML = item.scanned_date;

	
	//<th scope="col">Expiration Date</th>
	var cell = row.insertCell();
	cell.innerHTML =  item.expiration_date;
		  

 	var cell = row.insertCell();
	cell.innerHTML = '<input type="button" class = "edit_button" value="Edit" onclick="edit_item(this)">' ;

	var cell = row.insertCell();
	cell.innerHTML = '<input type="button" class = "delete_button" value="Delete" onclick="delete_item(this)">' ;
    // add the row to the end of the table body
	row.database_id = key;
	row.upc = item.upc;
    main_table.appendChild(row);
}

/*Delete row specified
inputs: index - index of row to be deleted*/
function delete_item(elem) {
	//Delete from the databse
	key = elem.parentNode.parentNode.database_id;
	console.log(key);
	var scanned_items_ref = db.collection("users").doc("DwPmSihSEJiwpZDycUnu").collection("scanned_items").doc(key);
	let deleteDoc = scanned_items_ref.delete();
	
	//delete from the table
	var main_table = elem.parentNode.parentNode.remove()
	renumber_table();
}

/*A special delete function for new items that have not yet been saved*/
function delete_new_item() {
//We don't have to delete anything in the database, as it has not been saved yet. So just delete from table.
	document.getElementById('main_table').deleteRow(1); //It's 1 because 0 is the header
	var add_button = document.getElementById('add_item_button');
	add_button.disabled = false;	
	var main_table = document.getElementById('main_table').getElementsByTagName('tbody')[0];
	for (var i = 0; i < main_table.rows.length; i++) {
		item_row = main_table.getElementsByTagName('tr')[i];		
		item_row.getElementsByTagName('td')[4].innerHTML = '<input class = "edit_button" type="button" value="Edit" onclick="edit_item(this)">' ;
		item_row.getElementsByTagName('td')[5].innerHTML = '<input class = "delete_button" type="button" value="Delete" onclick="delete_item(this)">' ;
	}
}

/*ALlow the user to edit any of the item's fields
inputs: index - index of row to be editted*/
function edit_item(elem) {
	var main_table = document.getElementById('main_table').getElementsByTagName('tbody')[0];
	for (var i = 0; i < main_table.rows.length; i++) {
			item_row = main_table.getElementsByTagName('tr')[i];	
			if (item_row != elem.parentNode.parentNode) {
				item_row.getElementsByTagName('td')[4].innerHTML = '<input class = "edit_button" disabled = "true" type="button" value="Edit" onclick="edit_item(this)">' ;
				item_row.getElementsByTagName('td')[5].innerHTML = '<input class = "delete_button" disabled = "true" type="button" value="Delete" onclick="delete_item(this)">' ;
			}
	}
	
	// get the reference for the body
	var row = elem.parentNode.parentNode;

	//<th scope="col">Name</th>
	row.getElementsByTagName('td')[0].setAttribute('contentEditable', 'true');

	//<th scope="col">Amount</th>
	row.getElementsByTagName('td')[1].setAttribute('contentEditable', 'true');

	//<th scope="col">Scanned Date</th>
	row.getElementsByTagName('td')[2].setAttribute('contentEditable', 'true');

	//<th scope="col">Expiration Date</th>
	row.getElementsByTagName('td')[3].setAttribute('contentEditable', 'true');	  

	row.getElementsByTagName('td')[4].innerHTML = '<input class="add_item_button" type="button" value="Save" onclick="save_item(this)">' ;	
}
/*ERROR*/
/*A special save button for new items*/
function save_new_item(elem) {
	var new_item_row = elem.parentNode.parentNode
	//Grab all values from the html UI
	var name = new_item_row.getElementsByTagName('td')[0].innerHTML;
	var amount = new_item_row.getElementsByTagName('td')[1].innerHTML;
	var scanned_date = new_item_row.getElementsByTagName('td')[2].innerHTML;
	var expiration_date = new_item_row.getElementsByTagName('td')[3].innerHTML;
	
	let data = {
	amount: amount,
	expiration_date: expiration_date,
	name: name,
	scanned_date: scanned_date,
	upc: 'N/A'
	};
	
	var scanned_items_ref = db.collection("users").doc("DwPmSihSEJiwpZDycUnu").collection("scanned_items").doc();
	new_item_row.database_id = scanned_items_ref.id;
	new_item_row.upc = 'N/A'
	scanned_items_ref.set(data);
	//Set all fields to be un-editable
	for (var i = 0; i < 4; i++) {
		var name = new_item_row.getElementsByTagName('td')[i].setAttribute('contentEditable', 'false');
	}
	
	new_item_row.getElementsByTagName('td')[4].innerHTML = '<input class = "edit_button" type="button" value="Edit" onclick="edit_item(this)">' ;
	new_item_row.getElementsByTagName('td')[5].innerHTML = '<input class = "delete_button" type="button" value="Delete" onclick="delete_item(this)">' ;
	
	//Renumber all items
	renumber_table();
	
	var add_button = document.getElementById('add_item_button');
	add_button.disabled = false;
	
	var main_table = document.getElementById('main_table').getElementsByTagName('tbody')[0];
	for (var i = 0; i < main_table.rows.length; i++) {
		item_row = main_table.getElementsByTagName('tr')[i];		
		item_row.getElementsByTagName('td')[4].innerHTML = '<input class = "edit_button" type="button" value="Edit" onclick="edit_item(this)">' ;
		item_row.getElementsByTagName('td')[5].innerHTML = '<input class = "delete_button" type="button" value="Delete" onclick="delete_item(this)">' ;
	}
}

/*Save any changes and updates the table and the database
inputs: index - index of row to be saved*/
function save_item(elem) {
//To-Do
	// get the reference for the body
	var row = elem.parentNode.parentNode;

	//<th scope="col">Name</th>
	var cell = row.getElementsByTagName('td')[0];
	cell.setAttribute('contentEditable', 'false');

	//<th scope="col">Amount</th>
	var cell = row.getElementsByTagName('td')[1];
	cell.setAttribute('contentEditable', 'false');

	//<th scope="col">Scanned Date</th>
	var cell = row.getElementsByTagName('td')[2];
	cell.setAttribute('contentEditable', 'false');

	//<th scope="col">Expiration Date</th>
	var cell = row.getElementsByTagName('td')[3];
	cell.setAttribute('contentEditable', 'false');	  

	var cell = row.getElementsByTagName('td')[4];
	cell.innerHTML = '<input class = "edit_button" type="button" value="Edit" onclick="edit_item(this)">' ;
	
	//var row = document.getElementById('main_table').getElementsByTagName('tbody')[0].getElementsByTagName('tr')[index];

	let data = {
	amount: 	row.getElementsByTagName('td')[1].innerHTML,
	expiration_date: 	row.getElementsByTagName('td')[3].innerHTML,
	name: 	row.getElementsByTagName('td')[0].innerHTML,
	scanned_date: 	row.getElementsByTagName('td')[2].innerHTML,
	upc: 	row.upc
	};
	
	var key = row.database_id;
	var scanned_items_ref = db.collection("users").doc("DwPmSihSEJiwpZDycUnu").collection("scanned_items").doc(key);
	scanned_items_ref.set(data);
	
	var main_table = document.getElementById('main_table').getElementsByTagName('tbody')[0];
	for (var i = 0; i < main_table.rows.length; i++) {
		
		item_row = main_table.getElementsByTagName('tr')[i];		
		item_row.getElementsByTagName('td')[4].innerHTML = '<input class = "edit_button" type="button" value="Edit" onclick="edit_item(this)">' ;
		item_row.getElementsByTagName('td')[5].innerHTML = '<input class = "delete_button" type="button" value="Delete" onclick="delete_item(this)">' ;
		console.log(i);		
	}
}

/*Add item button. Inserts a new row at the top of the table for the user to input the new item*/
function add_item() {
	//disable adding more than 1 item at a time
	var add_button = document.getElementById('add_item_button');
	add_button.disabled = true;
	// get the reference for the body
	
	var main_table = document.getElementById('main_table').getElementsByTagName('tbody')[0];	
	//disale all other buttons (reduces error cases)
	for (var i = 0; i < main_table.rows.length; i++) {
		item_row = main_table.getElementsByTagName('tr')[i];		
		item_row.getElementsByTagName('td')[4].innerHTML = '<input disabled = "true" class = "edit_button" type="button" value="Edit" onclick="edit_item(this)">' ;
		item_row.getElementsByTagName('td')[5].innerHTML = '<input disabled = "true" class = "delete_button" type="button" value="Delete" onclick="delete_item(this)">' ;
		
	}
	var row = main_table.insertRow(0);
	// creates a table row
	//var row = document.createElement("tr");


	//<th scope="col">Item</th>
	//var cell = row.insertCell();
	//cell.innerHTML = 0;

	//<th scope="col">Name</th>
	var cell = row.insertCell();
	cell.innerHTML = 'Name';
	cell.setAttribute('contentEditable', 'true');

	//<th scope="col">Amount</th>
	var cell = row.insertCell();
	cell.innerHTML = 'Amount';
	cell.setAttribute('contentEditable', 'true');

	//<th scope="col">Scanned Date</th>
	var cell = row.insertCell();
	cell.innerHTML = 'Scanned Date';
	cell.setAttribute('contentEditable', 'true');

	//<th scope="col">Expiration Date</th>
	var cell = row.insertCell();
	cell.innerHTML =  'Expiration Date';
	cell.setAttribute('contentEditable', 'true');	  

	var cell = row.insertCell();
	cell.innerHTML = '<input type="button" class = "add_item_button" value="Save" onclick="save_new_item(this)">' ;

	var cell = row.insertCell();
	cell.innerHTML = '<input type="button" class = "delete_button" value="Delete" onclick="delete_new_item(this)">' ;
	// add the row to the end of the table body
	//this.total_number_items = this.total_number_items + 1;
	
	
}

/* Re-index the rows, so that displayed item number is correct. ALso re-indexes the edit and delete, so that they pertain
to the correct */
function renumber_table() {
	var main_table = document.getElementById('main_table').getElementsByTagName('tbody')[0];
	for (var i = 0; i < main_table.rows.length; i++) {
		item_row = main_table.getElementsByTagName('tr')[i];
		//item_row.getElementsByTagName('td')[0].innerHTML = i + 1;		
		item_row.getElementsByTagName('td')[4].innerHTML = '<input class = "edit_button" type="button" value="Edit" onclick="edit_item(this)">' ;
		item_row.getElementsByTagName('td')[5].innerHTML = '<input class = "delete_button" type="button" value="Delete" onclick="delete_item(this)">' ;
		
	}
}


//retrieve_scanned_items()
//new_item_listener2()

var TableIDvalue = "main_table";
//////////////////////////////////////
/*ERROR: Sorting removes the databse_id of the row*/
//standardized date (YYYY-MM-DD)
var TableLastSortedColumn = -1;
function SortTable() {
	var sortColumn = parseInt(arguments[0]);
	var type = arguments.length > 1 ? arguments[1] : 'T';
	var dateformat = arguments.length > 2 ? arguments[2] : '';
	var table = document.getElementById(TableIDvalue);
	var tbody = table.getElementsByTagName("tbody")[0];
	var rows = tbody.getElementsByTagName("tr");
	var arrayOfRows = new Array();
	type = type.toUpperCase();
	dateformat = dateformat.toLowerCase();
	for(var i=0, len=rows.length; i<len; i++) {
		arrayOfRows[i] = new Object;
		arrayOfRows[i].oldIndex = i;
		var celltext = rows[i].getElementsByTagName("td")[sortColumn].innerHTML.replace(/<[^>]*>/g,"");
		if( type=='D' ) { arrayOfRows[i].value = GetDateSortingKey(dateformat,celltext); }
		else {
			var re = type=="N" ? /[^\.\-\+\d]/g : /[^a-zA-Z0-9]/g;
			arrayOfRows[i].value = celltext.replace(re,"").substr(0,25).toLowerCase();
			}
		}
	//If we are just clicking the same button twice, then you can just reverse them
	if (sortColumn == TableLastSortedColumn) { arrayOfRows.reverse(); }
	else {
		TableLastSortedColumn = sortColumn;
		switch(type) {
			case "N" : arrayOfRows.sort(CompareRowOfNumbers); break;
			case "D" : arrayOfRows.sort(CompareRowOfNumbers); break;
			default  : arrayOfRows.sort(CompareRowOfText);
			}
		}
	var newTableBody = document.createElement("tbody");
	for(var i=0, len=arrayOfRows.length; i<len; i++) {
		newTableBody.appendChild(rows[arrayOfRows[i].oldIndex].cloneNode(true));
		newTableBody.getElementsByTagName("tr")[i].database_id = rows[arrayOfRows[i].oldIndex].database_id;
		}
	table.replaceChild(newTableBody,tbody);
} // function SortTable()

function CompareRowOfText(a,b) {
	var aval = a.value;
	var bval = b.value;
	return( aval == bval ? 0 : (aval > bval ? 1 : -1) );
} // function CompareRowOfText()

function CompareRowOfNumbers(a,b) {
	var aval = /\d/.test(a.value) ? parseFloat(a.value) : 0;
	var bval = /\d/.test(b.value) ? parseFloat(b.value) : 0;
	return( aval == bval ? 0 : (aval > bval ? 1 : -1) );
} // function CompareRowOfNumbers()

function GetDateSortingKey(format,text) {
	if( format.length < 1 ) {
		return ""; 
	}
	format = format.toLowerCase();
	text = text.toLowerCase();
	text = text.replace(/^[^a-z0-9]*/,"");
	text = text.replace(/[^a-z0-9]*$/,"");
	
	if( text.length < 1 ) {
		return ""; 
	}
	text = text.replace(/[^a-z0-9]+/g,",");
	var date = text.split(",");
	if( date.length < 3 ) {
		return ""; 
	}
	
	var d=0, m=0, y=0;
	
	for( var i=0; i<3; i++ ) {
		var ts = format.substr(i,1);
		if( ts == "d" ) { d = date[i]; }
		else if( ts == "m" ) { m = date[i]; }
		else if( ts == "y" ) { y = date[i]; }
	}
	
	d = d.replace(/^0/,"");
	
	if( d < 10 ) {
		d = "0" + d; 
	}
	if( /[a-z]/.test(m) ) {
		m = m.substr(0,3);
		switch(m) {
			case "jan" : m = String(1); break;
			case "feb" : m = String(2); break;
			case "mar" : m = String(3); break;
			case "apr" : m = String(4); break;
			case "may" : m = String(5); break;
			case "jun" : m = String(6); break;
			case "jul" : m = String(7); break;
			case "aug" : m = String(8); break;
			case "sep" : m = String(9); break;
			case "oct" : m = String(10); break;
			case "nov" : m = String(11); break;
			case "dec" : m = String(12); break;
			default    : m = String(0);
			}
		}
	m = m.replace(/^0/,"");
	if( m < 10 ) {
		m = "0" + m; 
	}
	y = parseInt(y);
	if( y < 100 ) { 
		y = parseInt(y) + 2000; 
	}
	return "" + String(y) + "" + String(m) + "" + String(d) + "";
} // function GetDateSortingKey()

function timedRefresh(timeoutPeriod) {
	setTimeout("location.reload(true);", timeoutPeriod)
}



