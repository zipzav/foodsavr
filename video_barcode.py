# import the necessary packages
import time
import json
import requests

#from database import *
import constants

#Search upcitemdb for barcode provided.
#	input:
#		barcode - UPC to be searched
#	return:
#		name of product
def upcitemdb_lookup(barcode):
	try:
		response = requests.get("Https://api.upcitemdb.com/prod/trial/lookup?upc=" + barcode)
		response_json = response.json()
		if response_json['total'] >= 1:
			name = response_json["items"][0]["title"]
		else:
			name = "[ERROR]"
	except:
		name = "[ERROR]"
	finally:
		return name

#Search semantics for barcode provided.
#	input:
#		barcode - UPC to be searched
#	return:
#		name of product
def sem_lookup(barcode):
	try:
		sem3 = Products(api_key = "" ,api_secret = "")
		sem3.products_field("upc",barcode)
		response_json = sem3.get_products()
		if response_json['results_count'] >= 1:
			name = response_json["results"][0]["name"]
		else:
			name = "[ERROR]"
	except:
		name = "[ERROR]"
	finally:
		return name
	
#Search semantics for barcode provided.
#	input:
#		barcode - UPC to be searched
#	return:
#		name of product
def rpc_lookup(barcode):
	try:
		key = "d512b53ea322c4f1139afa85c9a002db58c959f4"
		s = xc.ServerProxy('https://www.upcdatabase.com/xmlrpc')
		params = {'rpc_key': key, 'upc': barcode}
		results = s.lookup(params)
		if results['message'] != 'Database entry found':
			name = "[ERROR]"
		elif results['status'] == 'fail':
			name = "[ERROR]"
		else:
			name = results['description']
	except:
			name = "[ERROR]"
	finally:
		return name


