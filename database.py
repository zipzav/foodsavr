import firebase_admin
from firebase_admin import credentials, firestore, db, auth
import datetime
from constants import *
# Initialize backend database
current_user_id = ''
db = ''
# Returns a reference to database client
def initializeDB():
	# Setup Firebase App
	global db
	cred = credentials.Certificate('foodsavr-f5a35-firebase-adminsdk-l0cwh-4fde91d245.json')

	firebase_admin.initialize_app(cred)
	db = firestore.client()

# Updates the expiry date of an item in the online list (hardcoded item right now)
# Returns nothing
#def updateExpiry(db, user_ref, expiry):
	#Get a reference to the food item "collection" and update the field with extracted expiry date from image
	#user_ref = db.collection(u'users').document(u'DwPmSihSEJiwpZDycUnu').collection(u'scanned_items').document(user_ref)
	#print(user_ref)
	#print(user_ref.get())
	#print('DATABASE ACCESS: ', expiry)
	#user_ref.update({
	#	'expiration_date': u'{}'.format(expiry)
	#})
	#return

#def sign_in():

#def sign_out():

def sign_up(email, password):
	return auth.create_user(email=email, password=password)
	
def delete_user(uid):
	auth.delete_user(user_record.uid, app=None)
	
#Add new item to database
#	inputs:
#		db = database instance
#		name = name of product
#		upc = upc of product
#	returns:
#	id of newly stored item
def storeItem(cabinet_code,upc, name):
	# Get the current date/time
	currentDT = datetime.datetime.now().date()
	
	#check if the item has been scanned before
	user_ref = db.collection(u'users').document(USER_DOCUMENT_ID).collection(u'cabinets').document(cabinet_code).collection(u'items').document(upc)
	if(user_ref.get().exists):
		new_quantity = int(user_ref.get().to_dict()['amount']) + 1
		user_ref.update({
		'amount': u'{}'.format(new_quantity),
		'scanned_date' : u'{}'.format(datetime.datetime.now())
	})
	else:
		user_ref.set({
			'amount' : u'1',
			'name': u'{}'.format(name),
			'upc': u'{}'.format(upc),
			'scanned_date' : u'{}'.format(datetime.datetime.now())
		})

#Add new item to database
#	inputs:
#		db = database instance
#		name = name of product
#		upc = upc of product
def isCabinetCode(upc):
	#global USER_DOCUMENT_ID
	user_ref = db.collection(u'users').document(USER_DOCUMENT_ID).collection(u'cabinets').document(upc).get()
	if(user_ref.exists):
		return True
	else:
		return False
		
def newbarcode_global(name, upc):
	currentDT = datetime.datetime.now()
	user_ref = db.collection(u'global_items').document(upc)
	user_ref = db.collection(u'global_items').document(upc).set({
		'name': u'{}'.format(name),
		'upc': u'{}'.format(upc),
		'date_added': u'{}'.format(datetime.datetime.now())
	})
	
#Search semantics for barcode provided.
#	input:
#		barcode - UPC to be searched
#	return:
#		name of product
def self_lookup(upc):
	user_ref = db.collection(u'global_items').document(upc).get()
	if(user_ref.exists):
		return user_ref.to_dict()['name']
	return "[ERROR]"

initializeDB()