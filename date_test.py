#I want to parse these dates to the Canadian Standard: Year Month Day

#1 EXP 2016 MAR 13 No need to parse
#2 EXP 13 MAR 2016
#3 EXP 16MA13   (YY/MM/DD) 
#4 EXP 17 AL 30 (/YY/MM/DD) 
#5 EXP /01/12/2019 (/MM/DD/YYYY)
#6 EXP /13/01/2019 (/DD/MM/YYYY)
#7 EXP /2019/12/10 (/YYYY/MM/DD)
#8 EXP 03 JAN 2010	(D/M/Y)


from datetime import datetime
import re
#Date formats supported. Order is important. First one found is used.
DATE_FORMATS = ['%Y %b %d', '%d %b %Y' ,  '%Y%b%d' , '%y%b%d', '%y %b %d', '%m/%d/%Y' , '%d/%m/%Y' , '%Y/%m/%d', '%d/%b/%Y']
# test_date = '10/01/2019'
# test_date = '01/13/2019' 
# test_date = '13 MAR 2016'
# test_date = '16DE13'
# test_date = '16 DE 13'
# test_date = '03 JAN 2010'
# test_date = '2019/12/10'
# test_date = '2019/12/10'
month_dict = {
  "JA" : "Jan",
  "Ja" : "Jan",
  "FE" : "Feb",
  "Fe" : "Feb",
  "MR" : "Mar",
  "Mr" : "Mar",
  "AP" : "Apr",
  "Ap" : "Apr",
  "AL" : "Apr",
  "Al" : "Apr",
  "MA" : "May",
  "Ma" : "May",
  "MY" : "May",
  "My" : "May",
  "JN" : "Jun",
  "Jn" : "Jun",
  "AU" : "Aug",
  "Au" : "Aug",
  "SE" : "Sep",
  "Se" : "Sep",
  "OC" : "Oct",
  "Oc" : "Oct",
  "NV" : "Nov",
  "Nv" : "Nov",
  "NO" : "Nov",
  "No" : "Nov",
  "DE" : "Dec",
  "De" : "Dec"
}
def convert_month(month):
  if month.group(0) in month_dict:
    return month_dict[str(month.group(0))]
  else: 
    return month.group(0)

def processInput(date):						
  new_date = re.sub("[a-zA-Z]{2,3}", convert_month, date)							
  return new_date

# Main function call
# input : date to standardize in string format
# output : standardized date (MM-DD-YYYY)
def standardize_date(test_date):
  for date_format in DATE_FORMATS:
      try:
      #Try to parse, if found, break out of loop
        my_date = datetime.strptime(processInput(test_date), date_format)
      except ValueError:
        pass
      else:
        formatted_date = datetime.strftime(my_date, '%m-%d-%Y')
        break
  else:
    formatted_date = "ERROR"
  #print( "-", my_date.month, "-" my_date.day, my_date.year)
  return formatted_date
# print(my_date.year)
# print(my_date.month)
# print(my_date.day)
# print(my_date.year, "-" my_date.month "-" my_date.day) # 2012-01-01


